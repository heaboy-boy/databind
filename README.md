# DATABIND-无sql绑定关联信息我们是认真的
## 绑定方式
### 数据字典关联绑定

>  当表中的字段为数据字典类型的值时，可使用数据字典关联来绑定表字段与数据字典的关联关系。
> 通过@BindDict注解，数据字典关联时无需写大量java代码和SQL关联查询，即可快速转换值字典value值为label。

* 使用@BindDict注解时需传两个参数，分别是type和field。

* type表示关联的数据字典类型；

* field表示关联字段。
  示例如下：

  ```java
  @BindDict(type="USER_STATUS", field = "status")
  private String statusLabel;
  ```

  

###  绑定从表Entity实体

> 绑定单个实体(或实体对应的VO)使用**@BindEntity**注解进行处理，将得到关联表对应的单个实体。

- 使用@BindEntity注解时需指定两个参数：entity和condition。
  - entity 表示关联实体类；
  - condition 表示关联条件（当前对象表中的字段加“'this.'前缀或写在比较条件的左侧，被绑定Entity表的字段在右侧）。
  - deepBind 是否深度绑定，默认false。当绑定Entity对象为VO(类定义中还有Bind注解)，开启deepBind=true，可以实现对被绑定VO的注解的再次绑定。
- 主表1-1直接关联从表，获取从表Entity，注解示例如下：

```java
@BindEntity(entity = Department.class, condition="this.department_id=id")
private Department department;
```

- deepBind 深度绑定注解示例如下：

```java
@BindEntity(entity = Department.class, condition="this.department_id=id", deepBind=true)
private DepartmentVO departmentVO;
```

### 绑定从表Entity实体列表

> 绑定实体(或实体对应的VO)列表使用**@BindEntityList**注解进行处理，将得到关联表对应的实体列表。

- @BindEntityList多数注解参数与@BindEntity相同。
  - 新增参数: orderBy 对绑定结果集合的排序标识，用法同列表页查询的orderBy排序，具体格式为'列名:排序方式,列名:排序方式'，示例如下：
    - orderBy="sort_id" //按sort_id升序（默认）
    - orderBy="sort_id:ASC" //按sort_id升序
    - orderBy="sort_id:DESC" //按sort_id降序
    - orderBy="type:ASC,id:DESC" //先按type升序，type相同按id降序
- 主表1-n直接关联从表，获取从表的Entity列表，注解示例如下：

```java
// 关联其他表
@BindEntityList(entity = Department.class, condition="department_id=id")
private List<Department> departmentList;

// 关联自身，实现加载子级
@BindEntityList(entity = Department.class, condition ="id=parent_id")
private List<Department> children;
 
```

###  绑定从表字段

> 绑定字段使用**@BindField**注解进行处理，将得到关联表的目标字段的值（集合）。

- @BindField 或 @BindFieldList 注解需三个参数，分别是entity、field、condition。
  - entity表示关联实体；
  - field表示关联表字段名；
  - condition表示关联条件，用法同BindEntity*。
- 主表1-1直接关联从表，获取从表字段值，注解示例如下：

```java
@BindField(entity=Department.class, field="name", condition="department_id=id AND parent_id>=0")
private String deptName;
 
```

## 绑定调用方式

1. 通过在VO类中添加相关字段，以及对应的关联绑定注解，来定义我们的绑定类型和需要得到的结果以及额外的条件等信息；
2. 注解添加后，调用**Binder**类中的相关方法(*bindRelations)执行这个绑定关系，目前提供了两种方式适配不同场景：

### 自动绑定关联

> 该关联会自动将相关信息查询并设置到voList中，适用于对已有的voList做处理，如：

```java
//List<MyUserVO> voList = ...; 
Binder.bindRelations(voList);
```

### 自动转型并绑定关联

> 该关联会自动将vo所继承的父类的实体列表进行绑定并自动转型为voList，适用于对于非voList的实体列表等做处理，如：

```java
// 查询单表获取Entity集合
// List<User> entityList = userService.list(queryWrapper);
// 转型并绑定关联
List<MyUserVO> voList = Binder.convertAndBindRelations(userList, MyUserVO.class);
```

### 通过BaseService接口实现绑定

```java
// 绑定单个VO对象
service.getViewObject()
// 绑定多个VO集合
service.getViewObjectList()
```

使用依赖

```xml
<dependency>
    <groupId>com.heaboy</groupId>
    <artifactId>databind</artifactId>
    <version>1.0.0</version>
    <exclusions>
        <!--如果使用spring boot 需要把面的依赖排除掉 -->
        <exclusion>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

从diboot-core 精简出来的仅供单个数据绑定的小工具,不支持中间表联查,涉及到join还是建议自行写sql而不是通过工具
本项目依赖mybatis-plus从diboot项目中精简得出,如果你是spring项目可以将spring的依赖排除掉
