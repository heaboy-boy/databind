/*
 * Copyright (c) 2015-2020, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.heaboy.databind.binding.parser;


import com.heaboy.databind.binding.binder.BaseBinder;
import com.heaboy.databind.binding.util.StringUtil;
import com.heaboy.databind.binding.util.CheckUtil;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.schema.Column;

import java.util.List;

/**
 * 条件表达式的管理器
 * @author mazc@dibo.ltd
 * @version v2.0
 * @date 2019/4/1
 */
@Slf4j
public class ConditionManager extends BaseConditionManager{

    /**
     * 附加条件到binder
     * @param condition
     * @param binder
     * @throws Exception
     */
    public static <T> void parseConditions(String condition, BaseBinder<T> binder) throws Exception {
        List<Expression> expressionList = getExpressionList(condition);
        if(CheckUtil.isEmpty(expressionList)){
            log.warn("无法解析注解条件: {} ", condition);
            return;
        }

        parseDirectRelation(binder, expressionList);

    }

    /**
     * 解析直接关联
     * @param binder
     * @param expressionList
     * @param <T>
     */
    private static <T> void parseDirectRelation(BaseBinder<T> binder, List<Expression> expressionList) {
        // 解析直接关联
        for(Expression operator : expressionList){
            if(operator instanceof EqualsTo){
                EqualsTo express = (EqualsTo)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                if(express.getRightExpression() instanceof Column){
                    String entityColumn = removeLeftAlias(express.getRightExpression().toString());
                    binder.joinOn(annoColumn, entityColumn);
                }
                else{
                    binder.andEQ(annoColumn, express.getRightExpression().toString());
                }
            }
            else if(operator instanceof NotEqualsTo){
                NotEqualsTo express = (NotEqualsTo)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                if(express.getRightExpression() instanceof Column){
                    binder.andApply(StringUtil.toSnakeCase(annoColumn) + " != " + StringUtil.toSnakeCase(express.getRightExpression().toString()));
                }
                else{
                    binder.andNE(annoColumn, express.getRightExpression().toString());
                }
            }
            else if(operator instanceof GreaterThan){
                GreaterThan express = (GreaterThan)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                if(express.getRightExpression() instanceof Column){
                    binder.andApply(StringUtil.toSnakeCase(annoColumn) + " > "+ StringUtil.toSnakeCase(express.getRightExpression().toString()));
                }
                else{
                    binder.andGT(annoColumn, express.getRightExpression().toString());
                }
            }
            else if(operator instanceof GreaterThanEquals){
                GreaterThanEquals express = (GreaterThanEquals)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                if(express.getRightExpression() instanceof Column){
                    binder.andApply(StringUtil.toSnakeCase(annoColumn) + " >= "+ express.getRightExpression().toString());
                }
                else{
                    binder.andGE(annoColumn, express.getRightExpression().toString());
                }
            }
            else if(operator instanceof MinorThan){
                MinorThan express = (MinorThan)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                if(express.getRightExpression() instanceof Column){
                    binder.andApply(StringUtil.toSnakeCase(annoColumn) + " < "+ express.getRightExpression().toString());
                }
                else{
                    binder.andLT(annoColumn, express.getRightExpression().toString());
                }
            }
            else if(operator instanceof MinorThanEquals){
                MinorThanEquals express = (MinorThanEquals)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                if(express.getRightExpression() instanceof Column){
                    binder.andApply(StringUtil.toSnakeCase(annoColumn) + " <= "+ express.getRightExpression().toString());
                }
                else{
                    binder.andLE(annoColumn, express.getRightExpression().toString());
                }
            }
            else if(operator instanceof IsNullExpression){
                IsNullExpression express = (IsNullExpression)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                if(express.isNot() == false){
                    binder.andIsNull(annoColumn);
                }
                else{
                    binder.andIsNotNull(annoColumn);
                }
            }
            else if(operator instanceof InExpression){
                InExpression express = (InExpression)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                if(express.isNot() == false){
                    binder.andApply(StringUtil.toSnakeCase(annoColumn) + " IN " + express.getRightItemsList().toString());
                }
                else{
                    binder.andApply(StringUtil.toSnakeCase(annoColumn) + " NOT IN " + express.getRightItemsList().toString());
                }
            }
            else if(operator instanceof Between){
                Between express = (Between)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                if(express.isNot() == false){
                    binder.andBetween(annoColumn, express.getBetweenExpressionStart().toString(), express.getBetweenExpressionEnd().toString());
                }
                else{
                    binder.andNotBetween(annoColumn, express.getBetweenExpressionStart().toString(), express.getBetweenExpressionEnd().toString());
                }
            }
            else if(operator instanceof LikeExpression){
                LikeExpression express = (LikeExpression)operator;
                String annoColumn = removeLeftAlias(express.getLeftExpression().toString());
                String value = express.getRightExpression().toString();
                if(express.isNot() == false){
                    binder.andLike(annoColumn, value);
                }
                else{
                    binder.andNotLike(annoColumn, value);
                }
            }
            else{
                log.warn("不支持的条件: "+operator.toString());
            }
        }
    }



    /**
     * 注解列
     * @return
     */
    private static String removeLeftAlias(String annoColumn){
        if(annoColumn.contains(".")){
            annoColumn = StringUtil.substringAfter(annoColumn, ".");
        }
        return annoColumn;
    }

}
