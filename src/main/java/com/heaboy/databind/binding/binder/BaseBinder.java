/*
 * Copyright (c) 2015-2020, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.heaboy.databind.binding.binder;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import com.heaboy.databind.binding.config.BaseConfig;
import com.heaboy.databind.binding.service.BaseService;
import com.heaboy.databind.binding.util.BeanUtil;
import com.heaboy.databind.binding.util.IGetter;
import com.heaboy.databind.binding.util.StringUtil;
import com.heaboy.databind.binding.util.CheckUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;

/**
 * 关系绑定Binder父类
 * @author mazc@dibo.ltd
 * @version v2.0
 * @date 2019/1/19
 */
public abstract class BaseBinder<T> {
    private static final Logger log = LoggerFactory.getLogger(BaseBinder.class);
    /***
     * 需要绑定到的VO注解对象List
     */
    protected List annoObjectList;
    /***
     * VO注解对象中的join on对象属性集合
     */
    protected List<String> annoObjJoinFlds;
    /***
     * DO对象中的关联join on对象属性集合
     */
    protected List<String> refObjJoinFlds;
    /**
     * 被关联对象的Service实例
     */
    protected IService<T> referencedService;
    /**
     * 初始化QueryWrapper
     */
    protected QueryWrapper<T> queryWrapper;

    protected Class<T> referencedEntityClass;

    /***
     * 构造方法
     * @param serviceInstance
     * @param voList
     */
    public BaseBinder(IService<T> serviceInstance, List voList){
        this.referencedService = serviceInstance;
        this.annoObjectList = voList;
        this.queryWrapper = new QueryWrapper<>();
        this.referencedEntityClass = BeanUtil.getGenericityClass(referencedService, 1);
        this.annoObjJoinFlds = new ArrayList<>(8);
        this.refObjJoinFlds = new ArrayList<>(8);
    }

    /**
     * join连接条件，指定当前VO的取值方法和关联entity的取值方法
     * @param annoObjectFkGetter 当前VO的取值方法
     * @param referencedEntityPkGetter 关联entity的取值方法
     * @param <T1> 当前VO的对象类型
     * @param <T2> 关联对象entity类型
     * @return
     */
    public <T1,T2> BaseBinder<T> joinOn(IGetter<T1> annoObjectFkGetter, IGetter<T2> referencedEntityPkGetter){
        return joinOn(BeanUtil.convertToFieldName(annoObjectFkGetter), BeanUtil.convertToFieldName(referencedEntityPkGetter));
    }

    /**
     * join连接条件，指定当前VO的取值方法和关联entity的取值方法
     * @param annoObjectForeignKey 当前VO的取值属性名
     * @param referencedEntityPrimaryKey 关联entity的属性
     * @return
     */
    public BaseBinder<T> joinOn(String annoObjectForeignKey, String referencedEntityPrimaryKey){
        if(annoObjectForeignKey != null && referencedEntityPrimaryKey != null){
            annoObjJoinFlds.add(StringUtil.toLowerCaseCamel(annoObjectForeignKey));
            refObjJoinFlds.add(StringUtil.toLowerCaseCamel(referencedEntityPrimaryKey));
        }
        return this;
    }

    public BaseBinder<T> andEQ(String fieldName, Object value){
        queryWrapper.eq(StringUtil.toSnakeCase(fieldName), formatValue(fieldName, value));
        return this;
    }
    public BaseBinder<T> andNE(String fieldName, Object value){
        queryWrapper.ne(StringUtil.toSnakeCase(fieldName), formatValue(fieldName, value));
        return this;
    }
    public BaseBinder<T> andGT(String fieldName, Object value){
        queryWrapper.gt(StringUtil.toSnakeCase(fieldName), formatValue(fieldName, value));
        return this;
    }
    public BaseBinder<T> andGE(String fieldName, Object value){
        queryWrapper.ge(StringUtil.toSnakeCase(fieldName), formatValue(fieldName, value));
        return this;
    }
    public BaseBinder<T> andLT(String fieldName, Object value){
        queryWrapper.lt(StringUtil.toSnakeCase(fieldName), formatValue(fieldName, value));
        return this;
    }
    public BaseBinder<T> andLE(String fieldName, Object value){
        queryWrapper.le(StringUtil.toSnakeCase(fieldName), formatValue(fieldName, value));
        return this;
    }
    public BaseBinder<T> andIsNotNull(String fieldName){
        queryWrapper.isNotNull(StringUtil.toSnakeCase(fieldName));
        return this;
    }
    public BaseBinder<T> andIsNull(String fieldName){
        queryWrapper.isNull(StringUtil.toSnakeCase(fieldName));
        return this;
    }
    public BaseBinder<T> andBetween(String fieldName, Object begin, Object end){
        queryWrapper.between(StringUtil.toSnakeCase(fieldName), formatValue(fieldName, begin), formatValue(fieldName, end));
        return this;
    }
    public BaseBinder<T> andLike(String fieldName, String value){
        fieldName = StringUtil.toSnakeCase(fieldName);
        value = (String)formatValue(fieldName, value);
        if(StringUtil.startsWith(value, "%")){
            value = StringUtil.substringAfter(value, "%");
            if(StringUtil.endsWith(value, "%")){
                value = StringUtil.substringBeforeLast(value, "%");
                queryWrapper.like(fieldName, value);
            }
            else{
                queryWrapper.likeLeft(fieldName, value);
            }
        }
        else if(StringUtil.endsWith(value, "%")){
            value = StringUtil.substringBeforeLast(value, "%");
            queryWrapper.likeRight(fieldName, value);
        }
        else{
            queryWrapper.like(fieldName, value);
        }
        return this;
    }
    public BaseBinder<T> andIn(String fieldName, Collection valueList){
        queryWrapper.in(StringUtil.toSnakeCase(fieldName), valueList);
        return this;
    }
    public BaseBinder<T> andNotIn(String fieldName, Collection valueList){
        queryWrapper.notIn(StringUtil.toSnakeCase(fieldName), valueList);
        return this;
    }
    public BaseBinder<T> andNotBetween(String fieldName, Object begin, Object end){
        queryWrapper.notBetween(StringUtil.toSnakeCase(fieldName), formatValue(fieldName, begin), formatValue(fieldName, end));
        return this;
    }
    public BaseBinder<T> andNotLike(String fieldName, String value){
        queryWrapper.notLike(StringUtil.toSnakeCase(fieldName), formatValue(fieldName, value));
        return this;
    }
    public BaseBinder<T> andApply(String applySql){
        queryWrapper.apply(applySql);
        return this;
    }


    /***
     * 执行绑定, 交由子类实现
     */
    public abstract void bind();
    /**
     * 构建join on
     */
    protected void buildQueryWrapperJoinOn(){
        for(int i = 0; i< annoObjJoinFlds.size(); i++){
            String annoObjJoinOnCol = annoObjJoinFlds.get(i);
            boolean[] hasNullFlags = new boolean[1];
            List annoObjectJoinOnList = BeanUtil.collectToList(annoObjectList, annoObjJoinOnCol, hasNullFlags);
            // 构建查询条件
            String refObjJoinOnCol = StringUtil.toSnakeCase(refObjJoinFlds.get(i));
            if(CheckUtil.isEmpty(annoObjectJoinOnList)){
                queryWrapper.isNull(refObjJoinOnCol);
            }
            else{
                // 有null值
                if(hasNullFlags[0]){
                    queryWrapper.and(qw -> qw.isNull(refObjJoinOnCol).or(w -> w.in(refObjJoinOnCol, annoObjectJoinOnList)));
                }
                else{
                    queryWrapper.in(refObjJoinOnCol, annoObjectJoinOnList);
                }
            }
        }
    }

    /**
     * 获取EntityList
     * @param queryWrapper
     * @return
     */
    protected List<T> getEntityList(Wrapper queryWrapper) {
        if(referencedService instanceof BaseService){
            return ((BaseService)referencedService).getEntityList(queryWrapper);
        }
        else{
            List<T> list = referencedService.list(queryWrapper);
            return checkedList(list);
        }
    }

    /**
     * 获取Map结果
     * @param queryWrapper
     * @return
     */
    protected List<Map<String, Object>> getMapList(Wrapper queryWrapper) {
        if(referencedService instanceof BaseService){
            return ((BaseService)referencedService).getMapList(queryWrapper);
        }
        else{
            List<Map<String, Object>> list = referencedService.listMaps(queryWrapper);
            return checkedList(list);
        }
    }
    /**
     * 从map中取值，如直接取为null尝试转换大写后再取，以支持ORACLE等大写命名数据库
     * @param map
     * @param key
     * @return
     */
    protected Object getValueIgnoreKeyCase(Map<String, Object> map, String key){
        if(key == null){
            return null;
        }
        if(map.containsKey(key)){
            return map.get(key);
        }
        if(map.containsKey(key.toUpperCase())){
            return map.get(key.toUpperCase());
        }
        return null;
    }
    /**
     * 从Map中提取ID的值
     * @param middleTableResultMap
     * @return
     */
    protected List extractIdValueFromMap(Map<String, List> middleTableResultMap) {
        List entityIdList = new ArrayList();
        for(Map.Entry<String, List> entry : middleTableResultMap.entrySet()){
            if(CheckUtil.isEmpty(entry.getValue())){
                continue;
            }
            for(Object id : entry.getValue()){
                if(!entityIdList.contains(id)){
                    entityIdList.add(id);
                }
            }
        }
        return entityIdList;
    }
    /**
     * 检查list，结果过多打印warn
     * @param list
     * @return
     */
    private List checkedList(List list){
        if(list == null){
            list = Collections.emptyList();
        }
        else if(list.size() > BaseConfig.getBatchSize()){
            log.warn("单次查询记录数量过大，返回结果数={}，请检查！", list.size());
        }
        return list;
    }
    /**
     * 格式化条件值
     * @param fieldName 属性名
     * @param value 值
     * @return
     */
    private Object formatValue(String fieldName, Object value){
        if(value instanceof String && StringUtil.contains((String)value, "'")){
            return StringUtil.replace((String)value, "'", "");
        }
        // 转型
        if(this.referencedEntityClass != null){
            Field field = BeanUtil.extractField(this.referencedEntityClass, StringUtil.toLowerCaseCamel(fieldName));
            if(field != null){
                return BeanUtil.convertValueToFieldType(value, field);
            }
        }
        return value;
    }
}