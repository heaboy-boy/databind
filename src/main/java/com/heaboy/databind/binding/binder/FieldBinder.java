/*
 * Copyright (c) 2015-2020, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.heaboy.databind.binding.binder;

import com.baomidou.mybatisplus.extension.service.IService;

import com.heaboy.databind.binding.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * 关联字段绑定
 * @author mazc@dibo.ltd
 * @version v2.0
 * @date 2019/1/19
 */
public class FieldBinder<T> extends BaseBinder<T> {
    private static final Logger log = LoggerFactory.getLogger(FieldBinder.class);

    /**
     * VO对象绑定赋值的属性名列表
     */
    protected List<String> annoObjectSetterPropNameList;
    /**
     * DO/Entity对象对应的getter取值属性名列表
     */
    protected List<String> referencedGetterColumnNameList;

    /***
     * 构造方法
     * @param serviceInstance
     * @param voList
     */
    public FieldBinder(IService<T> serviceInstance, List voList){
        super(serviceInstance, voList);
    }

    /***
     * 指定VO绑定属性赋值的setter和DO/Entity取值的getter方法
     * @param toVoSetter VO中调用赋值的setter方法
     * @param <T1> VO类型
     * @param <T2> DO类型
     * @param <R> set方法参数类型
     * @return
     */
    public <T1,T2,R> FieldBinder<T> link(IGetter<T2> fromDoGetter, ISetter<T1, R> toVoSetter){
        return link(BeanUtil.convertToFieldName(fromDoGetter), BeanUtil.convertToFieldName(toVoSetter));
    }

    /***
     * 指定VO绑定赋值的setter属性名和DO/Entity取值的getter属性名
     * @param toVoField VO中调用赋值的setter属性名
     * @return
     */
    public FieldBinder<T> link(String fromDoField, String toVoField){
        if(annoObjectSetterPropNameList == null){
            annoObjectSetterPropNameList = new ArrayList<>();
        }
        annoObjectSetterPropNameList.add(toVoField);
        if(referencedGetterColumnNameList == null){
            referencedGetterColumnNameList = new ArrayList<>();
        }
        referencedGetterColumnNameList.add(StringUtil.toSnakeCase(fromDoField));
        return this;
    }

    @Override
    public void bind() {
        if(CheckUtil.isEmpty(annoObjectList)){
            return;
        }
        if(CheckUtil.isEmpty(refObjJoinFlds)){
            log.warn("调用错误：无法从condition中解析出字段关联.");
            return;
        }
        if(referencedGetterColumnNameList == null){
            log.error("调用错误：字段绑定必须指定字段field");
            return;
        }
        // 直接关联
            this.buildSelectColumns();
            super.buildQueryWrapperJoinOn();
            // 获取匹配结果的mapList
            List<Map<String, Object>> mapList = getMapList(queryWrapper);
            if(CheckUtil.isEmpty(mapList)){
                return;
            }
            // 将结果list转换成map
            Map<String, Map<String, Object>> key2DataMap = this.buildMatchKey2ResultMap(mapList);
            // 遍历list并赋值
            for(Object annoObject : annoObjectList){
                String matchKey = buildMatchKey(annoObject);
                setFieldValueToTrunkObj(key2DataMap, annoObject, matchKey);
            }


    }

    /**
     * 设置字段值
     * @param key2DataMap
     * @param annoObject
     * @param matchKey
     */
    private void setFieldValueToTrunkObj(Map<String, Map<String, Object>> key2DataMap, Object annoObject, String matchKey) {
        Map<String, Object> relationMap = key2DataMap.get(matchKey);
        if (relationMap != null) {
            for (int i = 0; i < annoObjectSetterPropNameList.size(); i++) {
                Object valObj = getValueIgnoreKeyCase(relationMap, referencedGetterColumnNameList.get(i));
                BeanUtil.setProperty(annoObject, annoObjectSetterPropNameList.get(i), valObj);
            }
        }
    }

    /**
     * 构建匹配key-map目标的map
     * @param mapList
     * @return
     */
    protected Map<String, Map<String, Object>> buildMatchKey2ResultMap(List<Map<String, Object>> mapList){
        Map<String, Map<String, Object>> key2TargetMap = new HashMap<>(mapList.size());
        for(Map<String, Object> map : mapList){
            List<String> joinOnValues = new ArrayList<>(refObjJoinFlds.size());
            for(String refObjJoinOnCol : refObjJoinFlds){
                Object valObj = getValueIgnoreKeyCase(map, StringUtil.toSnakeCase(refObjJoinOnCol));
                joinOnValues.add(StringUtil.valueOf(valObj));
            }
            String matchKey = StringUtil.join(joinOnValues);
            if(matchKey != null){
                key2TargetMap.put(matchKey, map);
            }
        }
        return key2TargetMap;
    }

    /**
     * 构建匹配Key
     * @param annoObject
     * @return
     */
    private String buildMatchKey(Object annoObject){
        List<String> joinOnValues = new ArrayList<>(annoObjJoinFlds.size());
        for(String annoJoinOn : annoObjJoinFlds){
            // 将数子类型转换成字符串，以便解决类型不一致的问题
            String annoObjVal = BeanUtil.getStringProperty(annoObject, annoJoinOn);
            joinOnValues.add(annoObjVal);
        }
        return StringUtil.join(joinOnValues);
    }



        private void buildSelectColumns(){
            List<String> selectColumns = new ArrayList<>(referencedGetterColumnNameList.size()+1);
            for(String refObjJoinOn : refObjJoinFlds){
                selectColumns.add(StringUtil.toSnakeCase(refObjJoinOn));
            }
        selectColumns.addAll(referencedGetterColumnNameList);
        queryWrapper.select(StringUtil.toStringArray(selectColumns));
    }

}
