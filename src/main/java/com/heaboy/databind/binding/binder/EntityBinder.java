/*
 * Copyright (c) 2015-2020, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.heaboy.databind.binding.binder;

import com.baomidou.mybatisplus.extension.service.IService;

import com.heaboy.databind.binding.helper.ResultAssembler;
import com.heaboy.databind.binding.util.BeanUtil;
import com.heaboy.databind.binding.util.ISetter;
import com.heaboy.databind.binding.util.StringUtil;
import com.heaboy.databind.binding.util.CheckUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Entity实体绑定Binder，用于绑定当前一个entity到目标对象的属性
 * @author mazc@dibo.ltd
 * @version v2.0
 * @date 2019/1/19
 */
public class EntityBinder<T> extends BaseBinder<T> {
    private static final Logger log = LoggerFactory.getLogger(EntityBinder.class);

    /***
     * 给待绑定list中VO对象赋值的setter属性名
     */
    protected String annoObjectField;
    /***
     * 给待绑定list中VO对象赋值的setter属性class类型
     */
    protected Class<?> annoObjectFieldClass;

    /***
     * 构造方法
     * @param referencedService
     * @param voList
     */
    public EntityBinder(IService<T> referencedService, List voList){
        super(referencedService, voList);
    }

    /***
     * 指定VO绑定属性赋值的setter方法
     * @param voSetter VO中调用赋值的setter方法
     * @param <T1> VO类型
     * @param <R> set方法参数类型
     * @return
     */
    public <T1,R> BaseBinder<T> set(ISetter<T1, R> voSetter, Class annoObjectFieldClass){
        return set(BeanUtil.convertToFieldName(voSetter), annoObjectFieldClass);
    }

    /***
     * 指定VO绑定属性赋值的set属性
     * @param annoObjectField VO中调用赋值的setter属性
     * @return
     */
    public BaseBinder<T> set(String annoObjectField, Class annoObjectFieldClass){
        this.annoObjectField = annoObjectField;
        this.annoObjectFieldClass = annoObjectFieldClass;
        return this;
    }

    @Override
    public void bind() {
        if(CheckUtil.isEmpty(annoObjectList)){
            return;
        }
        if(CheckUtil.isEmpty(refObjJoinFlds)){
            log.warn("调用错误：无法从condition中解析出字段关联.");
            return;
        }
        // 直接关联Entity

        // @BindEntity(entity = Department.class, condition="this.department_id=id AND this.type=type")
        // Department department;
        super.buildQueryWrapperJoinOn();
        // 查询entity列表
        List<T> list = getEntityList(queryWrapper);
        if(CheckUtil.notEmpty(list)){
            Map<String, Object> valueEntityMap = this.buildMatchKey2EntityMap(list);
            ResultAssembler.bindPropValue(annoObjectField, annoObjectList, annoObjJoinFlds, valueEntityMap);
        }



    }

    /**
     * 构建匹配key-entity目标的map
     * @param list
     * @return
     */
    private Map<String, Object> buildMatchKey2EntityMap(List<T> list){
        Map<String, Object> key2TargetMap = new HashMap<>(list.size());
        List<String> joinOnValues = new ArrayList<>(refObjJoinFlds.size());
        for(T entity : list){
            joinOnValues.clear();
            for(String refObjJoinOnCol : refObjJoinFlds){
                String pkValue = BeanUtil.getStringProperty(entity, refObjJoinOnCol);
                joinOnValues.add(pkValue);
            }
            String matchKey = StringUtil.join(joinOnValues);

            Object target = entity;
            if(target instanceof Map == false){
                target = cloneOrConvertBean(entity);
            }
            key2TargetMap.put(matchKey, target);
        }
        return key2TargetMap;
    }

    /**
     * 克隆Entity/VO对象（如果与Entity类型不一致，如VO则先转型）
     * @param value
     */
    protected Object cloneOrConvertBean(T value){
        if(value == null){
            return value;
        }
        if(value.getClass().getName().equals(annoObjectFieldClass.getName())){
            return BeanUtil.cloneBean(value);
        }
        else{
            return BeanUtil.convert(value, annoObjectFieldClass);
        }
    }
}
