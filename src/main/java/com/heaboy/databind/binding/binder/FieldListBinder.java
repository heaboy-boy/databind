/*
 * Copyright (c) 2015-2020, www.dibo.ltd (service@dibo.ltd).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * <p>
 * https://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.heaboy.databind.binding.binder;

import com.baomidou.mybatisplus.extension.service.IService;

import com.heaboy.databind.binding.util.BeanUtil;
import com.heaboy.databind.binding.util.StringUtil;
import com.heaboy.databind.binding.util.CheckUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 关联字段绑定
 * @author mazc@dibo.ltd
 * @version v2.0
 * @date 2019/1/19
 */
public class FieldListBinder<T> extends FieldBinder<T> {
    private static final Logger log = LoggerFactory.getLogger(FieldListBinder.class);

    /***
     * 构造方法
     * @param serviceInstance
     * @param voList
     */
    public FieldListBinder(IService<T> serviceInstance, List voList) {
        super(serviceInstance, voList);
    }

    @Override
    public void bind() {
        if(CheckUtil.isEmpty(annoObjectList)){
            return;
        }
        if(CheckUtil.isEmpty(refObjJoinFlds)){
            log.warn("调用错误：无法从condition中解析出字段关联.");
            return;
        }
        if(referencedGetterColumnNameList == null){
            log.error("调用错误：字段绑定必须指定字段field");
            return;
        }
        Map<String, List> valueEntityListMap = new HashMap<>();
        List<String> selectColumns = new ArrayList<>(referencedGetterColumnNameList.size()+1);
        for(String refObjJoinOn : refObjJoinFlds){
            selectColumns.add(StringUtil.toSnakeCase(refObjJoinOn));
        }
        selectColumns.addAll(referencedGetterColumnNameList);
        queryWrapper.select(StringUtil.toStringArray(selectColumns));
        // 直接关联

            super.buildQueryWrapperJoinOn();
            // 查询entity列表: List<Role>
            List<T> list = getEntityList(queryWrapper);
            if(CheckUtil.notEmpty(list)){
                valueEntityListMap = this.buildMatchKey2FieldListMap(list);
            }
            // 遍历list并赋值
            bindPropValue(annoObjectList, annoObjJoinFlds, valueEntityListMap);


    }

    /***
     * 从对象集合提取某个属性值到list中
     * @param fromList
     * @param getterFields
     * @param valueMatchMap
     * @param <E>
     */
    public <E> void bindPropValue(List<E> fromList, List<String> getterFields, Map<String, List> valueMatchMap){
        if(CheckUtil.isEmpty(fromList) || CheckUtil.isEmpty(valueMatchMap)){
            return;
        }
        List<String> fieldValues = new ArrayList<>(getterFields.size());
        try{
            for(E object : fromList){
                fieldValues.clear();
                for(String getterField : getterFields){
                    String fieldValue = BeanUtil.getStringProperty(object, getterField);
                    fieldValues.add(fieldValue);
                }
                // 查找匹配Key
                String matchKey = StringUtil.join(fieldValues);
                List entityList = valueMatchMap.get(matchKey);
                if(entityList != null){
                    // 赋值
                    for(int i = 0; i< annoObjectSetterPropNameList.size(); i++){
                        List valObjList = BeanUtil.collectToList(entityList, StringUtil.toLowerCaseCamel(referencedGetterColumnNameList.get(i)));
                        BeanUtil.setProperty(object, annoObjectSetterPropNameList.get(i), valObjList);
                    }
                }
            }
        }
        catch (Exception e){
            log.warn("设置属性值异常", e);
        }
    }

    /***
     * 从对象集合提取某个属性值到list中
     * @param fromList
     * @param trunkObjColMapping
     * @param valueMatchMap
     * @param <E>
     */
    public <E> void bindPropValue(List<E> fromList, Map<String, String> trunkObjColMapping, Map<String, List> valueMatchMap){
        if(CheckUtil.isEmpty(fromList) || CheckUtil.isEmpty(valueMatchMap)){
            return;
        }
        List<String> fieldValues = new ArrayList<>(trunkObjColMapping.size());
        try{
            for(E object : fromList){
                fieldValues.clear();
                for(Map.Entry<String, String> entry :trunkObjColMapping.entrySet()){
                    String getterField = StringUtil.toLowerCaseCamel(entry.getKey());
                    String fieldValue = BeanUtil.getStringProperty(object, getterField);
                    fieldValues.add(fieldValue);
                }
                // 查找匹配Key
                String matchKey = StringUtil.join(fieldValues);
                List entityList = valueMatchMap.get(matchKey);
                if(entityList != null){
                    // 赋值
                    for(int i = 0; i< annoObjectSetterPropNameList.size(); i++){
                        List valObjList = BeanUtil.collectToList(entityList, StringUtil.toLowerCaseCamel(referencedGetterColumnNameList.get(i)));
                        BeanUtil.setProperty(object, annoObjectSetterPropNameList.get(i), valObjList);
                    }
                }
            }
        }
        catch (Exception e){
            log.warn("设置属性值异常", e);
        }
    }

    /**
     * 构建匹配key-entity目标的map
     * @param list
     * @return
     */
    private Map<String, List> buildMatchKey2FieldListMap(List<T> list){
        Map<String, List> key2TargetListMap = new HashMap<>(list.size());
        List<String> joinOnValues = new ArrayList<>(refObjJoinFlds.size());
        for(T entity : list){
            joinOnValues.clear();
            for(String refObjJoinOnCol : refObjJoinFlds){
                String fldValue = BeanUtil.getStringProperty(entity, refObjJoinOnCol);
                joinOnValues.add(fldValue);
            }
            String matchKey = StringUtil.join(joinOnValues);
            // 获取list
            List entityList = key2TargetListMap.get(matchKey);
            if(entityList == null){
                entityList = new ArrayList<>();
                key2TargetListMap.put(matchKey, entityList);
            }
            entityList.add(entity);
        }
        return key2TargetListMap;
    }

}
